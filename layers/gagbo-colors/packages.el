(setq gagbo-colors-packages
      '(
        (gorgeHousse-theme :location local)
        flucui-themes
        )
      )

(defun gagbo-colors/init-gorgeHousse-theme ()
  (require 'gorgeHousse-theme))
(defun gagbo-colors/init-flucui-themes ()
  nil)
